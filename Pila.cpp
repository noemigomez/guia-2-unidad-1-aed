#include <iostream>
#include "Pila.h"
using namespace std;

//Contructores
Pila::Pila(){
  // definición de atributos
  int max = 0;
  int tope = -1;
  string *pila = NULL;
  bool band = false;
}

void Pila::matriz(int max){
  this->max = max;
  /*iniciación de matriz*/
  this->pila = new string[max];
}

void Pila::pila_llena(){
  /*tope+1 porque los array se definen de tamaño 4, indices de 0 a 3
    si el tope+1 es igual al máximo permitido, band es true
    es verdad, la pila está llena*/
  if(this->tope+1==this->max){
    this->band = true;
  }
  else{
    this->band = false;
  }
}

void Pila::pila_vacia(){
  /*si el tope es -1 band es true, no hay elementos en la pilas
    es verdad, la pila está vacía*/
  if(this->tope==-1){
    this->band = true;
  }
  else{
    this->band = false;
  }
}

void Pila::pop(int can_pila){
  //can_pila, contenedor a eliminar
  pila_vacia();
  if(this->band){
    cout << "ERROR\nUnderflow, no podemos eliminar" << endl;
  }
  // si la pila no está vacía, se elimina
  else{
    this->pila[can_pila] = "\0";
    this->tope--;
  }
}

void Pila::push(string nombre){
  pila_llena();
  if(this->band){
    cout << "ERROR\nOverflow, no podemos agregar" << endl;
  }
  // si la pila no está llena, se agrega
  else{
    this->tope++;
    this->pila[this->tope] = nombre;
  }
}

string Pila::get_pila(int j){
  /*si el espacio está vacío*/
  if(this->pila[j] == "\0"){
    return "   ";
  }
  return this->pila[j];
}

int Pila::get_tope(){
  return this->tope;
}

int Pila::get_max(){
  return this->max;
}

#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
    private:
        int max = 0;
        int tope = -1;
        string *pila = NULL;
        bool band = false;

    public:
        // constructores
        Pila();
        Pila(int max);
        // métodos
        void matriz(int max);
        void pila_llena();
        void pila_vacia();
        void push(string nombre);
        void pop(int can_pila);
        string get_pila(int j);
        int get_tope();
        int get_max();



};
#endif

# Simulación Puerto Seco

El programa simula un puerto seco donde se guardan contenedores. La cantidad de pilas de contenedores y la cantidad de contenedores por pila son ingresados por usted, el usuario. Comienza con un puerto vacío donde se deben agregar contenedores, los cuales tienen nombre de 3 caracteres. Simula tanto el agregar contenedores a un puerto como su retiro. 

## Comenzando 

Comienza solicitando los valores de que tantos contenedores se pueden apilar en el puerto (m) y cuantas pilas pueden haber en él(n). Esto simula un cuadrilátero de lados n x m, con un área del mismo tamaño (si se pueden apilar 5 contenedores y pueden haber 4 pilas dentro del puerto, entonces habrá como mááximo 20 contenedores en el puerto). 

Al determinar esos datos entramos a crear un puerto vacío y se abre un menú con 4 opciones: agregar un contenedor, el cual pregunta en que pila desea agregarlo y su nombre, eliminar un contenedor, donde consulta cual de los contenedores desea eliminar, ver el puerto y salir.

El programa se detiene con cualquier dato mal ingresado y dentro del menú también con la opción 4.

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas 

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Pila.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

Comienza preguntando la cantidad máxima de contenedores que puede haber en una pila y posterior a eso la cantidad máxima de pilas que pueden haber en el puerto. En caso de ingresar uno de los datos de manera errónea, como negativos o caracteres simbolizará el fin automático del programa. 

Se crea de esta manera un puerto vacío que debe de ser llenado por usted.

Con todos los datos ingresados se abre un menú:

**1.Agregar contenedor:** Este solicitará en cual de las pilas disponibles del puerto desea agregar el contenedor, van desde 1 hasta el máximo que señaló en un principio. Luego de seleccionar la pila se solicita el nombre del contenedor el cual sugerimos que sea de 3 caracteres; puede ser del largo que desee pero para el momento donde se muestra el contenedor habrá una distribución asimétrica respecto a la vista. Si intenta agregar algo en una pila llena saldrá un error de Overflow. En el caso de agregar algo erróneo, como una letra cuando le solicitan una pila, un 0 o negativo, el programa se detendrá. 

**2. Eliminar contenedor:** Este solicita cual de todos los contenedores desea eliminar, lo busca y lo elimina. Muetra una secuencia de como se sacan los contenedores por encima del que quiere sacar y los mueve a otros espacios disponibles. Si el nombre ingresado no coincide con ninguno de los contenedores lo señala, si no hay espacio suficiente también. Si se pide eliminar un contenedor y existe más de uno con el mismo nombre se eliminará el último.

**3. Ver puerto:** Muestra el puerto y como está.

**4. Salir:** Termina el programa.

En las 3 primeras opciones se devuelve al menú por si quiere seguir realizando cosas con el puerto.

Cualquier dato que no coincida con lo solicitado significará el fin automático del programa.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream, unistd.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl 

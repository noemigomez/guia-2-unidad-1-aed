#include <iostream>
#include "Pila.h"
using namespace std;

#include <unistd.h>
unsigned int microseconds;

void imprimir_puerto(int n, int m, Pila puerto[]){
  for(int i=n-1; i>=0; i--){
    for(int j=0; j<m; j++){
      cout << "|" << puerto[j].get_pila(i) << "|";
    }
    cout << "\n";
  }

}

void eliminar_contenedor(int n, int m, Pila puerto[], int num_pila, int can_pila, int con_pila){
  // contar cuando no se encuentre más espacio
  int contador = 0;
  // ciclo hasta que no queden contenedores sobre el que se busca
  do{
    for(int i=0; i<m; i++){
      /*se deben remover contenedores por lo que no revisamos sobre la misma pila*/
      if(i!=num_pila){
        contador++;
        if(can_pila==0){
          break;
        }
        /*si la pila está en su tope o no
          tope+1 debido a que si son máximo 3 pilas, el tope será 2*/
        if((puerto[i].get_tope()+1) < puerto[i].get_max()){
          contador=0;
          // variable para el cambio de posición del contenedor
          string cambio = puerto[num_pila].get_pila(can_pila+con_pila);
          /*se agrega en espacio disponible, y luego se elimina
            esto simula movimiento*/
          puerto[i].push(cambio);
          puerto[num_pila].pop(can_pila+con_pila);
          // un contenedor menos para eliminar el buscado
          can_pila--;
          usleep(1000000);
          cout << "\nSe acaba de mover el contenedor " << cambio << endl;
          // se solicita una secuencia de como se mueven y se elimina, por lo tanto se imprime
          imprimir_puerto(n, m, puerto);
        }
      }
    }
    if(contador > m){
      cout << "No hay espacio suficiente en el puerto para eliminar el contenedor" << endl;
      break;
    }
  }while(can_pila!=0);
  if(contador<=m){
    usleep(1000000);
    /*cuando no queden contenedores encima, finalmente se elimina el contenedor deseado*/
    cout << "\nSe eliminó " << puerto[num_pila].get_pila(con_pila) << endl;
    puerto[num_pila].pop(con_pila);
    imprimir_puerto(n, m, puerto);
  }
}

void encontrar_contenedor(int n, int m, Pila puerto[]){
  string contenedor;
  cout << "Ingres nombre de contenedor a eliminar: ";
  cin >> contenedor;
  /* definición de variables a guardar respecto a datos del contenedor a eliminar
    can_pila = cantidad de contenedores sobre el que se desea sacar
    num_pila = en que pila se encontró el contenedor
    con_pila = posición dentro de la pila en la cual está el contenedor
    contador = cuantas veces se encuentra el contenedor
  */
  int can_pila, num_pila, con_pila;
  int contador = 0;
  for(int i=0; i<m; i++){
    for(int j=0; j<n; j++){
      if(puerto[i].get_pila(j)==contenedor){
        contador++;
        /*la cantidad de contenedores sobre el buscado es:
         el tope de la pila menos la posición del contenedor encontrado*/
        can_pila = puerto[i].get_tope() - j;
        num_pila = i;
        con_pila = j;
      }
    }
  }
  // si el contador es 0 es porque no se encontró el contenedor
  if(contador==0){
    cout << "Contenedor no encontrado" << endl;
  }
  else{
    eliminar_contenedor(n, m, puerto, num_pila, can_pila, con_pila);
  }
}

void llenar_puerto(int n, int m, Pila puerto[]){
  // en que pila se quiere agregar contenedor
  int pila;
  cout << "Inserte pila donde agregará contenedor: ";
  cin >> pila;

  if(pila <= m && pila > 0){
    string contenedor;
    cout << "Inserte nombre de contenedor: ";
    cin >> contenedor;

    /*si el arreglo es de tamaño 4, va de 0 a 3
    por eso si es la pila 1, tiene que ser en el arreglo puerto[0]*/
    puerto[pila-1].push(contenedor);
  }
  else{
    cout << "Esa pila no existe, por favor seleccione un número entre 1 y " << m << endl;
    llenar_puerto(n, m, puerto);
  }
}

void menu(int n, int m, Pila puerto[]){
  // para elegir desde el menú
  int opcion;
  do{
    cout << "[1] Agregar contenedor a puerto." << endl;
    cout << "[2] Eliminar contenedor de puerto." << endl;
    cout << "[3] Ver puerto." << endl;
    cout << "[4] Salir." << endl;
    cout << "Ingrese opción: ";
    cin >> opcion;

    if(opcion==1){
      llenar_puerto(n, m, puerto);
    }
    else if(opcion==2){
      encontrar_contenedor(n, m, puerto);
    }
    else if(opcion==3){
      imprimir_puerto(n, m, puerto);
    }
    else{
      break;
    }
  }while(opcion!=4);
}

int main()
{
  /*n: cuantas pilas pueden haber dentro del puerto
    m: cantidad máxima de contenedores a apilar */
  int n, m;
  cout << "Ingrese cantidad máxima de contenedores a apilar: ";
  cin >> n;
  cout << "Ingrese cantidad máxima de pilas de contenedores: ";
  cin >> m;

  /*deben ser números positivos*/
  if(n>0 && m>0){
    // creación del puerto
    Pila *puerto = new Pila[m];
    for(int i=0; i<m; i++){
      puerto[i].matriz(n);
    }

    cout << "Puerto creado vacío" << endl;
    menu(n, m, puerto);
  }


  return 0;
}
